$(document).ready(function () {

    $(".R_up").on("click", function () {
        let raise = $(this).next(".rating").text();
        +raise++;
        $(this).next(".rating").html(raise);
        let id = $(this).attr("id");
        let current = $(this).attr("title");
        console.log(raise);
        console.log(current);
        ajax(raise, 0, id, current);
    });

    $(".R_down").on("click", function () {
        let lower = $(this).prev(".rating").text();
        +lower--;
        $(this).prev(".rating").html(lower);
        let id = $(this).attr("id");
        let current = $(this).attr("title");
        console.log(lower);
        console.log(current);
        ajax(0, lower, id, current);
    });
});

function ajax(raise, lower, id, current) {
    $.ajax({
        url: 'local/raiting/refreshRaiting/refresh.php',
        type: 'POST',
        cache: false,
        data: {raise: raise, lower: lower, id: id, current: current},
        dataType: 'html',
        success: function (data) {
        }
    });
}


