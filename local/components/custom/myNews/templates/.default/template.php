<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php foreach ($arResult['tags'] as $tags){
    $tags_link = $tags['UF_TAG_LINK'];
    $tags_name = $tags['UF_NAME'];
    };?>
<pre><?var_dump($tags);?></pre>
<?$res_tags = array_combine($tags_link, $tags_name);?>

<div class="tabs">
    <div class="tabs__level tabs-level_top tabs-menu">
        <a href="https://habr.com/ru/hub/php/" class="tabs-menu__item tabs-menu__item_link" rel="nofollow"  >
            <h3 class="tabs-menu__item-text tabs-menu__item-text_active">
                Все подряд

            </h3>
        </a>
        <a href="https://habr.com/ru/hub/php/top/" class="tabs-menu__item tabs-menu__item_link" rel="nofollow"  >
            <h3 class="tabs-menu__item-text ">
                Лучшие

            </h3>
        </a>
        <a href="https://habr.com/ru/hub/php/authors/" class="tabs-menu__item tabs-menu__item_link" rel="nofollow"  >
            <h3 class="tabs-menu__item-text ">
                Авторы

            </h3>
        </a>
    </div>

    <div class="tabs__level tabs__level_bottom">
        <ul class="toggle-menu ">
            <li class="toggle-menu__item">
                <a href="/?clear_cache=Y" class="toggle-menu__item-link toggle-menu__item-link_active" rel="nofollow" title="Все публикации в хронологическом порядке">
                    Без порога
                </a>
            </li>
            <li class="toggle-menu__item">
                <a href="/?RATING=9&clear_cache=Y" class="toggle-menu__item-link " rel="nofollow" title="Все публикации с рейтингом 10 и выше">
                    ≥10
                </a>
            </li>
            <li class="toggle-menu__item">
                <a href="/?RATING=24&clear_cache=Y" class="toggle-menu__item-link " rel="nofollow" title="Все публикации с рейтингом 25 и выше">
                    ≥25
                </a>
            </li>
            <li class="toggle-menu__item">
                <a href="/?RATING=49&clear_cache=Y" class="toggle-menu__item-link " rel="nofollow" title="Все публикации с рейтингом 50 и выше">
                    ≥50
                </a>
            </li>
            <li class="toggle-menu__item">
                <a href="/?RATING=99&clear_cache=Y" class="toggle-menu__item-link " rel="nofollow" title="Все публикации с рейтингом 100 и выше">
                    ≥100
                </a>
            </li>
        </ul>
    </div>
</div>

        <div class="posts_list">
<?php
foreach ($arResult['news'] as $news):
$arResult['TIMESTAMP_X'] = ConvertDateTime($arResult["TIMESTAMP_X"], "  d F Y  HH:MI");
$users= CUser::GetByID($news['CREATED_BY']); // перенести в компонент там собрать в массив -> резалт
$user = $users->Fetch();
?>
<ul class="content-list shortcuts_items">
    <li class="content-list__item content-list__item_post shortcuts_item" id="post_462737">
        <article class="post post_preview" lang="ru">

                <header class="post__meta">
                    <a href="https://habr.com/ru/users/caballero/" class="post__user-info user-info" title="Автор публикации">
                        <img src="//habrastorage.org/getpro/habr/avatars/727/ec4/712/727ec471257576d343224fc19705421e.jpg"
                        width="24" height="24" class="user-info__image-pic user-info__image-pic_small">
                        <span class="user-info__nickname user-info__nickname_small"><?=$user['LAST_NAME']?> <?=$user['NAME']?></span>
                    </a>
                    <span class="post__time" data-time_published="2019-07-23T07:54Z"><?=$news['TIMESTAMP_X']?></span>
                </header>

                <h2 class="post__title">
                    <a href="https://habr.com/ru/post/462737/" class="post__title_link"><?=$news['NAME']?></a>
                </h2>
            <!-- START highload block -->
            <ul class="post__hubs inline-list">
                <?php foreach ($res_tags as $key => $value): ?>
                <li class="inline-list__item inline-list__item_hub">
                    <a href="<?=$key?>" class="inline-list__item-link hub-link "
                       rel="nofollow" title="Вы не подписаны на этот хаб"> <?=$value?>
                    </a>
                </li>
                <?php endforeach; ?>
            </ul>
            <!-- END highload block -->
                <ul class="post__marks inline-list"><li class="inline-list__item inline-list__item_post-type">
                    <span class="post__type-label" title="Обучающий материал">Tutorial</span></li>
                    <li class="inline-list__item inline-list__item_post-type">
                        <a href="https://habr.com/ru/info/help/karma/#recovery" class="post__type-label"
                            title="Реабилитационный пост от пользователя с отрицательной кармой">Recovery Mode
                        </a>
                    </li>
                </ul>
                <div class="post__body post__body_crop ">
                    <div class="post__text post__text-html js-mediator-article">
                        <img src="<?= CFile::GetPath($news['PREVIEW_PICTURE'])?>"><?=$news['PREVIEW_TEXT']?>
                        <br>
                    </div>
                        <a class="btn btn_x-large btn_outline_blue post__habracut-btn" href="/posts/<?=$news['ID']?>">Читать дальше →</a>
                </div>

                <footer class="post__footer">
                    <ul class="post-stats  js-user_" data-post-type="publish_ugc" id="infopanel_post_462737">
                        <li class="post-stats__item post-stats__item_voting-wjt">
                            <div class="voting-wjt voting-wjt_post js-post-vote" data-id="462737" data-type="2">
                                <button type="button" class="R_up btn btn-default" title="<?=$news['PROPERTY_RATING_VALUE']?>" id="<?=$news['ID']?> "><img src="/local/templates/main/assets/img/up.png" width="10" height="16" alt="повысить рейтинг"/></button>
                                    <span class="rating voting-wjt__counter voting-wjt__counter_positive"><?=$news['PROPERTY_RATING_VALUE']?></span>
                                <button type="button" class="R_down btn btn-default" title="<?=$news['PROPERTY_RATING_VALUE']?>" id="<?=$news['ID']?>"><img src="/local/templates/main/assets/img/down.png" width="10" height="16" alt="понизить рейтинг"/></button>
                            </div>
                        </li>
                        <li class="post-stats__item post-stats__item_bookmark">
                            <button type="button" class="btn bookmark-btn bookmark-btn_post " data-post-type="publish_ugc"
                                data-type="2" data-id="462737" data-action="add" title="Только зарегистрированные пользователи могут добавлять публикации в закладки"
                                onclick="posts_add_to_favorite(this);">
                                <span class="btn_inner"><svg class="icon-svg_bookmark" width="10" height="16">
                                <use xlink:href="https://habr.com/images/1565282437/common-svg-sprite.svg#book" /></svg>
                                <span class="bookmark__counter js-favs_count" title="Количество пользователей, добавивших публикацию в закладки">4</span></span>
                            </button>
                        </li>
                        <li class="post-stats__item post-stats__item_views">
                            <div class="post-stats__views" title="Количество просмотров">
                                <svg class="icon-svg_views-count" width="21" height="12">
                                <use xlink:href="https://habr.com/images/1565282437/common-svg-sprite.svg#eye" /></svg>
                                <span class="post-stats__views-count">387</span>
                            </div>
                        </li>
                        <li class="post-stats__item post-stats__item_comments">
                            <a href="https://habr.com/ru/post/462737/#comments" class="post-stats__comments-link" rel="nofollow">
                                <svg class="icon-svg_post-comments" width="16" height="16">
                                <use xlink:href="https://habr.com/images/1565282437/common-svg-sprite.svg#comment" /></svg>
                                <span class="post-stats__comments-count" title="Читать комментарии">4</span>
                            </a>
                        </li>
                    </ul>
                </footer>
        </article>
    </li>
</ul>

<?php
    endforeach;
    $arResult['res']->NavPrint("События", false, "", "/bitrix/modules/statistic/admin/adv_navprint.php"); ?>
</div>
</div>
