<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;
use Bitrix\Main\Loader;
/**
 * Class setnews
 * CBitrixComponent - является оболочкой компонента. Для каждого подключаемого компонента создаётся свой экземпляр класса
 */
class SetNews extends CBitrixComponent
{
    public function executeComponent()
    {
        Loader::includeModule('iblock');
        Loader::includeModule("highloadblock");

        $news = [];
        $elements = [];
        $hl = 5;
        $tags  = [];

        function GetEntityDataClass($HlBlockId) {
            if (empty($HlBlockId) || $HlBlockId < 1)
            {
                return false;
            }
            $hlblock = HLBT::getById($HlBlockId)->fetch();
            $entity = HLBT::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();
            return $entity_data_class;
        }

        //$cache - экземпляр класса который, будет кешировать объекты
        $cache = new CPHPCache;
        $cache_life = 86400;

            // если кеш есть и он ещё не истек то
        if ($cache->InitCache($cache_life, '', "/myNews/")) {
            // получаем закешированные переменные
            $elements = $cache->GetVars()['elements'];

        } else {
            ($cache->StartDataCache());
            // запрашиваем из бд все элементы нажного инфоблока
            $news = CIBlockElement::GetList(
                [
                    'CREATED' => "ASC",
                ],
                [
                    'IBLOCK_CODE' => 'news',
                    '>PROPERTY_RATING' => $_GET['RATING'],
                ],
                false,
                [
                    "nPageSize" => 3,
                    "bShowAll" => true,
                ],

                [
                    'ID', 'IBLOCK_ID', 'NAME', 'PREVIEW_PICTURE',
                    'PREVIEW_PICTURE', 'DETAIL_TEXT', 'PREVIEW_TEXT',
                    'PROPERTY_LINK_1', 'PROPERTY_LINK_2',
                    'PROPERTY_LINK_3', 'PROPERTY_NAME_LINK_1','PROPERTY_NAME_LINK_2',
                    'PROPERTY_NAME_LINK_3','PROPERTY_LINK_TITLE',
                    'PROPERTY_LINK_MORE', 'TIMESTAMP_X', 'PROPERTY_RATING',
                    'PROPERTY_NEGATIVE_CLICK','PROPERTY_POSITIVE_CLICK', 'CREATED_BY'
                ]
            );

            while ($element = $news->Fetch()) {
                $elements[] = $element;
            }

            $cache->EndDataCache(['elements' => $elements]);
            $cache->EndDataCache(['news' => $news]);

            $entity_data_class = GetEntityDataClass($hl);
            $rsData = $entity_data_class::getList(array(
                'select' => array('*'),
            ));
            while($el = $rsData->fetch()){
                $tags[] = $el;
            }

            $news->NavStart(0);
        }

        $this->arResult['news'] = $elements;
        $this->arResult['res'] = $news;
        $this->arResult['tags'] = $tags;
        $this->includeComponentTemplate();

    }
}

