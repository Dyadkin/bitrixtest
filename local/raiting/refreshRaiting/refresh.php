<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");
    $raise = $_POST['raise'];
    $lower = $_POST['lower'];
    $id = $_POST['id'];
    $current = (int)$_POST['current'];

    if($raise == 0){
        $raise_res = 0;
    }
    else {
        $raise_res = $raise - $current;
    }

    if($lower == 0){
        $lower_res = 0;
    }
    else {
        $lower_res = $current - $lower;
    }

        $rating  = $current +  $raise_res - $lower_res;

    CIBlockElement::SetPropertyValuesEx(
        $id,
        false,
        [
            "RATING" => $rating,
        ]
    );

echo json_encode(['rating' => $rating]);